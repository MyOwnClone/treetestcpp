#include <string>
#include <iostream>
#include "tree.h"

struct intVariable
{
	std::string name;
	int			value;

	intVariable()
	{
		value = 0;
	}

	intVariable(std::string name, int value)
	{
		this->name	= name;
		this->value = value;
	}

	bool operator<(const intVariable& other)
	{
		return (value < other.value);
	}

	
	bool operator==(const intVariable& other) const
	{
		return (name == other.name && value == other.value);
	}
};

void dump(core::tree<intVariable>::iterator iter)
{
	for (auto innerIter = iter.begin(); innerIter != iter.end(); innerIter++)
	{
		std::cout << innerIter.level() << " : " << innerIter.data().name << std::endl;

		dump(innerIter);
	}
}

int main()
{
	core::tree<intVariable> t;
	*t = intVariable("root", 0);

	auto iter1 = t.push_back(intVariable("10", 1));
	auto iter2 = t.push_back(intVariable("11", 2));

	iter1.push_back(intVariable("20", 3));
	iter1.push_back(intVariable("21", 4));

	iter2.push_back(intVariable("22", 5));
	auto innerIter = iter2.push_back(intVariable("23", 6));

	dump(t);

	iter2.remove(innerIter.data());

	std::cout << "after removal:" << std::endl;

	dump(t);

	core::tree<intVariable> treeCopy = t;
	std::cout << "copy:" << std::endl;

	dump(treeCopy);

	auto toChange = treeCopy.tree_find_breadth(intVariable("22", 5));
	toChange.clear_tree();

	std::cout << "after removal from copy:" << std::endl;

	dump(treeCopy);
}